FROM maven:3-jdk-11 

ADD target/*.jar /home/*.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","/home/*.jar"]
