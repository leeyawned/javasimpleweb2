# Java Simple Web 2 Project

## Application
This is a simple Java Web Application that just displays Hello World. Using the Spring Boot Framework, we are able to create an application that runs on port 8080 for easy access.

## CI/CD Pipeline
What is required is that the source code has to be tested and scanned for quality and security issues. We can automate this scanning and testing phases with this pipeline to automatically trigger when a commit is made and check the output.

## Deployment
We will then deploy the checked application to AWS EKS. We are assuming that the cluster has already been created.

## Final testing
Since the website URL is always the same, we can test the application to see if there are any vulnerabilities on the application side.
