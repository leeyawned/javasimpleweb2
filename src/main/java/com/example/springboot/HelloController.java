package com.example.springboot;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("/")
    public String index() {
        // change from Hello World to Hello World 2
        return "<body style='background-color:black;'><div style='display:flex; align-items: center; justify-content:center; height:100vh; color:white; font-size: 100px;'>Hello World 2!</div></body>";
    }
}
